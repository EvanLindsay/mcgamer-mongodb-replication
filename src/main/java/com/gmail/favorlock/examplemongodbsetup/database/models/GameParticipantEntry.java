package com.gmail.favorlock.examplemongodbsetup.database.models;

import lombok.Getter;
import org.mongodb.morphia.annotations.Reference;

public class GameParticipantEntry {

    public GameParticipantEntry(PlayerProfile player, int kills, int chests, long lifespan) {
        this.player = player;
        this.kills = kills;
        this.chests = chests;
        this.lifespan = lifespan;
    }

    @Reference
    @Getter
    private PlayerProfile player;

    @Getter
    private int kills = 0;

    @Getter
    private int chests = 0;

    @Getter
    private long lifespan = 0;

}
