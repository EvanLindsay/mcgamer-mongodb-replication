package com.gmail.favorlock.examplemongodbsetup.database;

import com.gmail.favorlock.examplemongodbsetup.database.dao.GameEntryDao;
import com.gmail.favorlock.examplemongodbsetup.database.dao.PlayerProfileDao;
import com.gmail.favorlock.examplemongodbsetup.database.models.GameEntry;
import com.gmail.favorlock.examplemongodbsetup.database.models.PlayerProfile;
import com.mongodb.WriteConcern;
import org.bukkit.entity.Player;

public class DataApi {

    public static PlayerProfile loadPlayerProfile(String uuid, String name) {
        PlayerProfile profile = PlayerProfileDao.getInstance().findOne("uuid", uuid);

        if (profile == null) {
            profile = new PlayerProfile(uuid);
        }

        updatePlayerInfo(profile, name);
        savePlayerProfile(profile);

        return profile;
    }

    public static PlayerProfile getPlayerProfile(Player player) {
        return loadPlayerProfile(player.getUniqueId().toString(), null);
    }

    public static PlayerProfile getPlayerProfileFromName(String name) {
        for (PlayerProfile profile : PlayerProfileDao.getInstance().createQuery().field("name").containsIgnoreCase(name)) {
            if (profile.getName().equalsIgnoreCase(name)) {
                return profile;
            }
        }

        return null;
    }

    public static void updatePlayerInfo(PlayerProfile profile, String name) {
        if (name != null)
            profile.setName(name);
    }
    
    public static void savePlayerProfile(PlayerProfile profile) {
        for (GameEntry entry : profile.getStats().getGames()) {
            if (entry.getId() == null) {
                GameEntryDao.getInstance().save(entry, WriteConcern.FSYNC_SAFE);
            }
        }

        PlayerProfileDao.getInstance().save(profile, WriteConcern.FSYNC_SAFE);
    }

}
