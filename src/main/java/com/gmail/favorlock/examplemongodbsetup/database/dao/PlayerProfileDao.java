package com.gmail.favorlock.examplemongodbsetup.database.dao;

import com.gmail.favorlock.examplemongodbsetup.database.models.PlayerProfile;
import com.gmail.favorlock.examplemongodbsetup.database.models.PlayerStats;
import lombok.Getter;
import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;

public class PlayerProfileDao extends BasicDAO<PlayerProfile, ObjectId> {

    @Getter private static PlayerProfileDao instance;

    public PlayerProfileDao(Datastore ds) {
        super(ds);
        instance = this;
    }

}
