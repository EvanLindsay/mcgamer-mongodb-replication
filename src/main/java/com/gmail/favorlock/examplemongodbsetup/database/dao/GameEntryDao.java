package com.gmail.favorlock.examplemongodbsetup.database.dao;

import com.gmail.favorlock.examplemongodbsetup.database.models.GameEntry;
import lombok.Getter;
import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;

public class GameEntryDao extends BasicDAO<GameEntry, ObjectId> {

    @Getter private static GameEntryDao instance;

    public GameEntryDao(Datastore ds) {
        super(ds);
        instance = this;
    }

}
