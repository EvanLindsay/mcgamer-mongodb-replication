package com.gmail.favorlock.examplemongodbsetup.database.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Indexed;
import org.mongodb.morphia.annotations.Reference;
import org.mongodb.morphia.utils.IndexDirection;

import java.util.ArrayList;
import java.util.List;

public class PlayerStats {

    @Indexed(value = IndexDirection.DESC, name = "wins")
    @Getter @Setter
    private int wins = 0;

    @Getter @Setter
    private int totalGames = 0;

    @Getter @Setter
    private int mostKills = 0;

    @Getter @Setter
    private int totalKills = 0;

    @Getter @Setter
    private int totalPoints = 0;

    @Getter @Setter
    private int totalChests = 0;

    @Getter @Setter
    private long longestLife = 0;

    @Getter @Setter
    private long totalLife = 0;

    @Reference
    @Getter
    private List<GameEntry> games = new ArrayList<>();

}
