package com.gmail.favorlock.examplemongodbsetup.database.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

import java.util.ArrayList;
import java.util.List;

@Entity(value = "stats.games", noClassnameStored = true)
@NoArgsConstructor
public class GameEntry {

    public GameEntry(String server, String game, int gameNumber) {
        this.server = server;
        this.game = game;
        this.gameNumber = gameNumber;
    }

    @Id
    @Getter
    private ObjectId id;

    @Getter
    private String server = "";

    @Getter
    private String game = "";

    @Getter
    private int gameNumber = 0;

    @Embedded
    @Getter
    private List<GameParticipantEntry> participants = new ArrayList<>();

}
