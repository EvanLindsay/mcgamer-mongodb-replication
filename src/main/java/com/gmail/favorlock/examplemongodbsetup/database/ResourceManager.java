package com.gmail.favorlock.examplemongodbsetup.database;

import com.gmail.favorlock.examplemongodbsetup.ExampleMongoDbSetup;
import com.gmail.favorlock.examplemongodbsetup.config.Settings;
import com.gmail.favorlock.examplemongodbsetup.database.dao.GameEntryDao;
import com.gmail.favorlock.examplemongodbsetup.database.dao.PlayerProfileDao;
import com.gmail.favorlock.examplemongodbsetup.database.models.PlayerProfile;
import com.gmail.favorlock.examplemongodbsetup.database.models.PlayerStats;
import com.gmail.favorlock.quickmongo.DatabaseObject;
import com.gmail.favorlock.quickmongo.MongoResource;
import lombok.Getter;
import org.mongodb.morphia.Datastore;

import java.util.HashSet;

public class ResourceManager {

    @Getter private MongoResource resource;
    @Getter private Datastore datastore;

    public ResourceManager() {
        if (resource == null) {
            resource = new MongoResource(getDatabaseObject(), ExampleMongoDbSetup.getInstance().getLogger());
        }

        datastore = getResource().getDatastore(new HashSet<Class>(ExampleMongoDbSetup.getInstance().getDatabaseClasses()));
        ensureIndexes(datastore);
        registerDao(datastore);
    }

    private DatabaseObject getDatabaseObject() {
        Settings settings = ExampleMongoDbSetup.getSettings();

        DatabaseObject databaseObject = new DatabaseObject(settings.host, settings.port);

        if (settings.username.equals("") == false && settings.password.equals("") == false) {
            databaseObject.withUsername(settings.username);
            databaseObject.withPassword(settings.password);
        }

        if (settings.database.equals("") == false) {
            databaseObject.withDatabase(settings.database);
        }

        return databaseObject;
    }

    private void ensureIndexes(Datastore datastore) {
        datastore.ensureIndexes(PlayerProfile.class);
        datastore.ensureIndexes(PlayerStats.class);
    }

    private void registerDao(Datastore datastore) {
        new PlayerProfileDao(datastore);
        new GameEntryDao(datastore);
    }

}
