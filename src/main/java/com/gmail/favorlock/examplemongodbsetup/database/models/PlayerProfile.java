package com.gmail.favorlock.examplemongodbsetup.database.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.*;
import org.mongodb.morphia.utils.IndexDirection;

@Entity(value = "profiles", noClassnameStored = true)
@NoArgsConstructor
public class PlayerProfile {

    public PlayerProfile(String uuid) {
        this.uuid = uuid;
    }

    @Id
    @Getter
    private ObjectId id;

    @Indexed(value = IndexDirection.ASC, unique = true)
    @Getter
    private String uuid;

    @Indexed(value = IndexDirection.ASC)
    @Getter @Setter
    private String name = "";

    @Embedded
    @Getter
    private PlayerStats stats = new PlayerStats();

}
