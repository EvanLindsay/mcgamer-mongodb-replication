package com.gmail.favorlock.examplemongodbsetup;

import com.gmail.favorlock.examplemongodbsetup.config.Settings;
import com.gmail.favorlock.examplemongodbsetup.database.ResourceManager;
import com.gmail.favorlock.examplemongodbsetup.database.models.GameEntry;
import com.gmail.favorlock.examplemongodbsetup.database.models.PlayerStats;
import com.gmail.favorlock.examplemongodbsetup.listeners.PlayerListener;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;

public class ExampleMongoDbSetup extends JavaPlugin {

    @Getter private static ExampleMongoDbSetup instance;
    @Getter private static Settings settings;
    @Getter private static ResourceManager resourceManager;

    public void onEnable() {
        instance = this;
        settings = new Settings(this);

        if (settings.initialize(this) == false) {
            disable();
            return;
        }

        resourceManager = new ResourceManager();

        registerListeners();
    }

    private void registerListeners() {
        Bukkit.getPluginManager().registerEvents(new PlayerListener(), this);
    }

    private void disable() {
        Bukkit.getPluginManager().disablePlugin(this);
    }

    public List<Class<?>> getDatabaseClasses() {
        List<Class<?>> list = new ArrayList<>();

        list.add(PlayerStats.class);
        list.add(GameEntry.class);

        return list;
    }

}
