package com.gmail.favorlock.examplemongodbsetup.listeners;

import com.gmail.favorlock.examplemongodbsetup.database.DataApi;
import com.gmail.favorlock.examplemongodbsetup.database.models.PlayerProfile;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerListener implements Listener {

    @EventHandler(priority = EventPriority.LOWEST)
    public void onJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        PlayerProfile profile = DataApi.loadPlayerProfile(player.getUniqueId().toString(), event.getPlayer().getName());
    }

}
